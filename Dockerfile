FROM atwalter/acl2s_gradescope_autograder

RUN apt-get update && apt-get install -y curl unzip dos2unix && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /autograder 

COPY . /autograder/

RUN dos2unix /autograder/run_autograder /autograder/setup.sh

RUN apt-get update && bash /autograder/setup.sh &&  apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /autograder

# Certify books
#RUN ${ACL2_SYSTEM_BOOKS}/build/cert.pl dfa.lisp

# Build executable
#RUN acl2s < dfa_equiv.lsp
#RUN acl2s < dfa_run.lsp
#RUN acl2s < pda.lisp
RUN acl2s < tm.lisp
