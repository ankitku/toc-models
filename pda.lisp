(in-package "ACL2S")
(include-book "interface/top")

(defdata state                var)
(defdata states               (listof state))
(defdata element              (oneof character var nat :e))
(defdata word                 (listof element))
(defdata-alias alphabet       word)
(defdata-alias stack-alphabet word)
(defdata-alias stack          word)
(defdata t-domain             (list state element element))
(defdata t-range              (listof (list state element)))
(defdata transition-fun       (alistof t-domain t-range))

(check= (statep 'even) t)
(check= (elementp '#\() t)
(check= (t-domainp '(q0 a z0)) t)
(check= (t-rangep '((q0 #\( ))) t)
(check= (transition-funp  '(((q0 #\( #\( ) . ((q0 #\( )))
			    ((q0 #\( z0  ) . ((q0 #\( )))))
        t)

(check= (transition-funp '(((q0 :e :e) . ((q1 b)))
                   ((q1 0   :e) . ((q1 0)))
                   ((q1 1   0)   . ((q2 :e)))
                   ((q2 1   0)   . ((q2 :e)))
                   ((q2 :e b)   . ((q3 :e)))))
        t)

(definecd word-car (w :word) :element
  (match w
    (() :e)
    ((e . &) e)))

(defdata pda (list states         ;all states
                   alphabet       ;alphabet
		   stack-alphabet ;alphabet
                   transition-fun ;transition function
                   state          ;start state
		   states         ;accepting states
                   ))

(definecd pda-states (d :pda) :states
  (car d))

(definecd pda-alphabet (d :pda) :alphabet
  (second d))

(definecd pda-stack-alphabet (d :pda) :stack-alphabet
  (third d))

(definecd pda-trans (d :pda) :transition-fun
  (fourth d))

(definecd pda-start (d :pda) :state
  (fifth d))

(definecd pda-accept (d :pda) :states
  (sixth d))

(defconst *pda-test*
  (list '(q0 q1 q2)
        '(#\( #\) )
	'(z0  #\( )
	'(((q0 :e :e ) . ((q1 z0  )))
          ((q1 #\( :e ) . ((q1 #\( )))
	  ((q1 #\) #\( ) . ((q1 :e)))
	  ((q1 :e z0 )  . ((q2 :e) (q0 :e))))
	'q0
	'(q2)))


(check= (pdap *pda-test*) t)

(defdata [statexstackxword] (listof (list state stack word)))

(definecd pda-trx-lookup (trans :transition-fun dom :t-domain) :t-range
  (match trans
    (() '())
    (((!dom . val) . &) val)
    ((& . rst) (pda-trx-lookup rst dom))))

;;converts a list of statexelement pairs to a list of statexstack pairs
(definecd es->stks (rs :t-range stk :stack w :word) :[statexstackxword]
  (match rs
    (() '())
    (((s :e) . tl) (cons (list s (cdr stk) w)
                          (es->stks tl stk w)))
    (((s e) . tl) (cons (list s (cons e (cdr stk)) w)
                        (es->stks tl stk w)))))

(defthm app-[statexstackxword]
  (=> (and ([statexstackxword]p r) ([statexstackxword]p s))
      ([statexstackxword]p (append r s))))

(definecd pdastep (trans :transition-fun in :[statexstackxword])
  :[statexstackxword]
  (match in
    (() '())
    (((s stk w) . tl) (append (es->stks (pda-trx-lookup trans `(,s ,(word-car w) ,(word-car stk)))
                                        stk
                                        (cdr w)) ;; one letter was used up
                              ;;stack epsilon transition
                              (es->stks (pda-trx-lookup trans `(,s ,(word-car w) :e))
                                        (cons :e stk)
                                        (cdr w))
                              ;;word epsilon transition
                              (es->stks (pda-trx-lookup trans `(,s :e ,(word-car stk)))
                                        stk
                                        w) ;;word remains unchanged
                              ;;stack and word eps transition
                              (es->stks (pda-trx-lookup trans `(,s :e ,(word-car stk)))
                                        (cons :e stk)
                                        w)
                              (pdastep trans tl)))))

(definecd accepted (res :[statexstackxword] accept-states :states) :boolean
  (if (endp res)
      nil
    (or (and (endp (third (car res))) ;;no remaining word
             (in (first (car res)) accept-states)) ;;accept state reached
        (accepted (cdr res) accept-states))))

(property rem-dups-statexstackxword (res :[statexstackxword])
          ([statexstackxword]p (remove-duplicates-equal res)))
  
(definecd run-steps (n :nat pda :pda sstws :[statexstackxword]) :boolean
  (cond
   ((== n 0) nil)
   ((accepted sstws (pda-accept pda)) t)
   (t (run-steps (1- n) pda (remove-duplicates-equal (pdastep (pda-trans pda) sstws))))))

(definecd run-pda (pda :pda w :word) :boolean
  (run-steps 100 pda `(,(list (pda-start pda) nil w))))

(check= (run-pda *pda-test* '( #\( #\) )) t)
(check= (run-pda *pda-test* '( #\( #\) #\( #\( #\) #\) #\( #\( #\( #\) #\) #\) )) t)
(check= (run-pda *pda-test* '( #\( #\( )) nil)     
(check= (run-pda *pda-test* '( #\( #\) #\( )) nil)     

;; utility functions
(definecd subset (a :tl b :tl) :bool
  (cond ((endp a) t)
	((in (car a) b) (subset (cdr a) b))
	(t nil)))

(check= (subset '(1 1 3) '(1 2 3)) t)

:q
(in-package "ACL2S")
(declaim (optimize (safety 3) (speed 0) (space 0) (debug 3)))
(declaim (sb-ext:muffle-conditions style-warning))


;;------------------------------------------------------------------------
;; Symbol name manipulation
;;------------------------------------------------------------------------

(defun gen-symb (x n)
  (read-from-string (format nil x n)))

(defun gen-sym-pred (x)
  (gen-symb "~ap" x))

(defun gen-symb-const (x)
  (gen-symb "*~a*" x))

(gen-sym-pred 'pda)

;;------------------------------------------------------------------------
;; PDA make/reset events generation
;;------------------------------------------------------------------------

(defun reset-history ()
  (acl2s-event `acl2s::(ubt 1)))

(defun reset-pda-def (d)
  (acl2s-event `acl2s::(ubt ',(gen-sym-pred d))))

(defun error-and-reset (msg def)
  (progn (reset-pda-def def)
	 (error (format nil "[~a]" msg))
         (sb-ext:exit)))


(defun query-function-has-start (txf start-state)
  (in (list start-state :e :e) (strip-cars txf) :test 'equal))

(defun query-function-start-state-adds-base (txf start-state)
  (not (endp (remove :e (mapcar 'second (pda-trx-lookup txf (list start-state :e :e)))))))

(defun query-function-distinctdom (txf)
  (let ((domain (strip-cars txf)))
    (not (== (len domain)
             (len (remove-duplicates domain :test 'equal))))))

(defun query-extra-functiondom (state-def elem-def stk-elem-def txf)
  (let ((es1 (gen-sym-pred elem-def))
        (es2 (gen-sym-pred stk-elem-def))
	(ss (gen-sym-pred state-def))
        (doma (strip-cars txf)))
    (itest?-query
     `acl2s::(=> (in state-element-pair= (quote ,doma))
                 (and  (,ss (first state-element-pair=))
                       (,es1 (second state-element-pair=))
                       (,es2 (third state-element-pair=)))))))

;; generates defdata events while also checking if input is actually a PDA
(defun mk-pda-events (name states alphabet stack-alphabet
			   start-state accept-states transition-fun)
  (let* ((p-state (gen-symb "~a-state" name))
	 (p-states (gen-symb "~a-states" name))
	 (p-elem (gen-symb "~a-element" name))
	 (p-word (gen-symb "~a-word" name))
	 (p-ab (gen-symb "~a-alphabet" name))
	 (p-stk-elem (gen-symb "~a-stk-element" name))
	 (p-stk-word (gen-symb "~a-stk-word" name))
	 (p-stk-ab (gen-symb "~a-stk-alphabet" name))
	 (p-tdom (gen-symb "~a-t-domain" name))
	 (p-trange (gen-symb "~a-t-range" name))
	 (p-f (gen-symb "~a-transition-function" name))
	 (p-fp (gen-sym-pred p-f))
         (pda-name (gen-symb-const name)))
    (acl2s-event `acl2s::(defdata ,p-state  (enum (quote ,states))))
    (if (not (statesp `acl2s::,states))
	(error-and-reset "incorrect states" p-state)
      (progn (acl2s-event `acl2s::(defdata ,p-states (listof ,p-state)))
	     (acl2s-event `acl2s::(defdata ,p-elem  (enum (quote ,alphabet))))
	     (acl2s-event `acl2s::(defdata ,p-word (listof ,p-elem)))
	     (acl2s-event `acl2s::(defdata ,p-ab ,p-word))
             (acl2s-event `acl2s::(defdata ,p-stk-elem  (enum (quote ,stack-alphabet))))
             (acl2s-event `acl2s::(defdata ,p-stk-word (listof ,p-stk-elem)))
             (acl2s-event `acl2s::(defdata ,p-stk-ab ,p-stk-word))
	     (if (not (in start-state `acl2s::,states))
		 (error-and-reset "incorrect start state" p-state)
	       (progn
		 (if (not (subset `acl2s::,accept-states `acl2s::,states))
		     (error-and-reset "incorrect accept states" p-state)
		   (progn 
		     (acl2s-event `acl2s::(defdata ,p-tdom (list ,p-state ,p-elem ,p-stk-elem)))
                     (acl2s-event `acl2s::(defdata ,p-trange (listof (list ,p-state ,p-stk-elem))))
		     (acl2s-event `acl2s::(defdata ,p-f (alistof ,p-tdom ,p-trange)))
                     (let ((res (query-function-distinctdom transition-fun)))
                       (if res (error-and-reset "transition function domain is not distinct." p-state)
                         (let ((res (query-extra-functiondom p-state p-elem p-stk-elem transition-fun)))
                           (if (car res)
                               (error-and-reset (format nil "Domain of transition function is not of type : states x input alphabet x stack alphabet ~a." (cdr res)) p-state)
                             (if (not (query-function-has-start transition-fun start-state))
                                 (error-and-reset (format nil "Starting
transition from (~a :e :e) missing from the transition function." start-state) p-state)
                               (if (not (query-function-start-state-adds-base transition-fun start-state))
                                 (error-and-reset (format nil "Start state
transition does not add a base stack symbol" start-state) p-state)
                             ;;(if (not (second (acl2s-compute `acl2s::(,d-fp (quote ,transition-fun)))))
                             ;; with all the other checks we added for functions, we do not really need to enforce it as a map
                             ;;  (error-and-reset "incorrect transition function" d-state)
                                 (progn (acl2s-event `acl2s::(defconst ,pda-name (list ',states ',alphabet ',stack-alphabet ',transition-fun ',start-state ',accept-states)))
                                        (cons t (format nil "Legal PDA : ~a" `acl2s::(,states ,alphabet ,stack-alphabet ,transition-fun ,start-state ,accept-states))))))))))))))))))
  

(defun gen-pda-fn (&key name states alphabet stack-alphabet start-state accept-states transition-fun)
  (let* ((df `acl2s::(,states ,alphabet ,stack-alphabet ,transition-fun ,start-state ,accept-states)))
    (mk-pda-events name states alphabet stack-alphabet start-state accept-states transition-fun)))

(defmacro gen-pda (&key name states alphabet stack-alphabet start-state accept-states transition-fun)
  (unless name (error "name missing"))
  (unless states (error "states missing"))
  (unless alphabet (error "alphabet missing"))
  (unless stack-alphabet (error "stack alphabet missing"))
  (unless start-state (error "start state missing"))
  (unless accept-states (error "accept states missing"))
  (unless transition-fun (error "transition-fun missing"))
  (let ((up-ab (cons :e alphabet))
        (up-stkab (cons :e stack-alphabet)))
  `(gen-pda-fn :name ',name
	       :states ',states
	       :alphabet ',up-ab
	       :stack-alphabet ',up-stkab
	       :start-state ',start-state
	       :accept-states ',accept-states
	       :transition-fun ',transition-fun)))



(gen-pda
 :name zn1n
 :states (q0 q1 q2 q3)
 :alphabet (0 1)
 :stack-alphabet (0 z)
 :start-state q0
 :accept-states (q3)
 :transition-fun (((q0 :e :e) . ((q1 z)))
                  ((q1 0  :e) . ((q1 0)))
                  ((q1 1   0)   . ((q2 :e)))
                  ((q2 1   0)   . ((q2 :e)))
                  ((q2 :e z)   . ((q3 :e)))))


(run-pda *ZN1N* '(0 0 1 1))
(run-pda *ZN1N* '(0 0 1 1 1))
(run-pda *ZN1N* '(0 0 0 0 0 0 0  0 1 1 1 1 1 1 1 1 1))
(run-pda *ZN1N* '(0 0 0 0 0 0 0  0 1 1 1 1 1 1 1 1 1 0))
(run-pda *ZN1N* '(0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1))
(run-pda *ZN1N* '())


(gen-pda
 :name pda1
 :states (q0 q1 q2 q3)
 :alphabet (0 1)
 :stack-alphabet (0 b)
 :start-state q0
 :accept-states (q0 q3)
 :transition-fun (((q0 :e :e) . ((q1 b)))
                  ((q1 :e 0) . ((q1 0)))
                  ((q1 0   :e) . ((q1 0)))
                  ((q1 1   0)   . ((q2 :e)))
                  ((q2 1   0)   . ((q2 :e)))
                  ((q2 :e b)   . ((q3 :e)))))

(run-pda *pda1* '(0 0 0 0 0 0 0 1 1 1 1 1 1 1))

(gen-pda
 :name pda2
 :states (q0 q1 q2 q3)
 :alphabet (0 1)
 :stack-alphabet (0 b)
 :start-state q0
 :accept-states (q3)
 :transition-fun (((q0 :e :e) . ((q1 b)))
                  ((q1 :e 0) . ((q1 0)))
                  ((q1 0   :e) . ((q1 0)))
                  ((q2 :e 0) . ((q2 0)))
                  ((q2 0   :e) . ((q2 0)))
                  ((q1 1   0)   . ((q2 :e)))
                  ((q2 1   0)   . ((q2 :e)))
                  ((q2 :e b)   . ((q3 :e)))))


(run-pda *pda2* '(0 0 0 0 0 0 0 1 1 1 1 1 1 1))
(time (run-pda *pda2* '(0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1)))
(time (run-pda *pda2* '(0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1)))

;;run-steps is set to explore a depth of 100 in the execution tree.
;;having more depth will require more time to find accepting states.


;;------------------------------------------------------------------------
;; DFA check events generation
;;------------------------------------------------------------------------

(defun query-alphabet-equal (pda1-name pda2-name)
  (let ((da1 (gen-symb "~a-alphabet" pda1-name))
	(da2 (gen-symb "~a-alphabet" pda2-name)))
    (acl2s-event
     `acl2s::(defdata-equal-strict ,da1 ,da2))))

(defun query-equivalence (pda1-name pda2-name)
  (let ((res (query-alphabet-equal pda1-name pda2-name))
	(dn (gen-symb "~a-wordp" pda1-name))
	(pda1 (gen-symb-const pda1-name))
	(pda2 (gen-symb-const pda2-name)))
    (unless (boundp pda1)
      (reset-and-exit :msg (format nil "Undefined PDA ~a" pda1-name)
                      :func query-equivalence))
    (unless (boundp pda2)
      (reset-and-exit :msg (format nil "Undefined PDA ~a" pda2-name)
                      :func query-equivalence))
    (if (car res)
	(cons nil "Incorrect alphabet provided.")
      (let ((res (itest?-query
                  `acl2s::(=> (,dn w)
                              (== (run-pda ,pda1 w)
                                  (run-pda ,pda2 w))))))
        (if (car res)
            (cons nil (format nil "Transition function error. The following words
  were misclassified :~% ~a" (substitute :e nil (mapcar #'cadar (car (cdr res))))
  (gen-symb "~a-state" pda2-name)))
          (cons t (format nil "~a is correct." pda2-name)))))))

(gen-pda
 :name instr
 :states (q0 q1 q2 q3)
 :alphabet (0 1)
 :stack-alphabet (0 z)
 :start-state q0
 :accept-states (q0 q3)
 :transition-fun (((q0 :e :e) . ((q1 z)))
                  ((q1 0  :e) . ((q1 0)))
                  ((q1 1   0)   . ((q2 :e)))
                  ((q2 1   0)   . ((q2 :e)))
                  ((q2 :e z)   . ((q3 :e)))))

(gen-pda
 :name student
 :states (q0 q1 q2 q3)
 :alphabet (0 1)
 :stack-alphabet (0 z)
 :start-state q0
 :accept-states (q3)
 :transition-fun (((q0 :e :e) . ((q1 z)))
                  ((q1 0  :e) . ((q1 0)))
                  ((q1 1   0)   . ((q2 :e)))
                  ((q2 1   0)   . ((q2 :e)))
                  ((q2 :e z)   . ((q3 :e)))))


;(query-equivalence 'instr 'student)
(run-pda *instr* nil)
(run-pda *student* nil)

;;------------------------------------------------------------------------
;; PAPER EXAMPLES
;;------------------------------------------------------------------------

(save-exec "pda_run_exec" nil
           :init-forms '((set-gag-mode nil)
                         (value :q))
           :toplevel-args "--eval '(declaim (sb-ext:muffle-conditions style-warning))' --eval '(acl2s::main (cdr sb-ext:*posix-argv*))' --disable-debugger")



