#|

 PETE: I modified Ankit's version to allow one to compare DFAs with
 different alphabets and sketched out aspects of the user interface,
 i.e., how a user defines DFAs and also how one can automatically test
 and automate the reasoning of equivalence of DFAs.

 Here are some roughly written todos: 

 1. Write a macro that generates all of the stuff below.

 Something like

 (gen-dfa
  :name           *dfa1*
  :states         (even odd)
  :alphabet       (0 1)
  :start          even
  :accept         (odd)
  :transition-fun ((even 0 even)
    	           (even 1 odd)
 	           (odd  0 odd)
 	           (odd  1 even)))

 will do the following.

 A. Check that name is a new name, types, that transitions is
    complete and unambiguous, etc.

 B. Generate all the data, function definitions. 

 2. Define a generic dfa recognizer.

 The problem right now is that a user will have to define run-dfa on
 their own because the states, etc can be different. So, a generic
 version may allow any atoms, or symbols, or whatever as states and
 similarly for elements. Then the dfa-specific types get automatically
 generated. Make sure you can compare 2 DFAs that are supposed to
 accept the same language but have different states and transitions
 with the user only having to define the dfa, as in the defconst
 below. 

 From an implementation point of view, you can do all of this in
 ACL2s. See defunc.lisp and definec.lisp for examples, or you can do
 this in lisp using the ACL2s interface functions and the z3 code and
 ccg are good examples of how to do that. I would go this route. So,
 define a lisp function gen-dfa that takes input as shown above, does
 checking in lisp and generates the appropriate acl2s-events. You can
 turn that into an ACL2s function.


|#

; This is a supertype of DFAs. One can change some of the data
; definition, eg, one could allow states to be numbers or lists or
; whatever

(defdata state          var)
(defdata states         (listof state))
(defdata element        all)
(defdata word           (listof element))
(defdata alphabet       word) 
(defdata t-domain       (list state element))
(defdata transition-fun (map t-domain state))

(check= (statep 'even) t)
(check= (elementp 1)   t)
(check= (transition-funp  '(((even 1) . odd))) t)


(defdata dfa (list states         ;all states
                   alphabet       ;alphabet
                   transition-fun ;transition function
                   state          ;start state
                   states         ;accepting states
                   ))

(definec dfa-states (d :dfa) :states
  (car d))

(definec dfa-alphabet (d :dfa) :alphabet
  (second d))

(definec dfa-trans (d :dfa) :transition-fun
  (third d))

(definec dfa-start (d :dfa) :state
  (fourth d))

(definec dfa-accept (d :dfa) :states
  (fifth d))

(defconst *dfa-test*
  (list '(even odd)
        '(0 1)
        '(((even 0) . even)
	  ((even 1) . odd)
	  ((odd  0) . odd)
	  ((odd  1) . even))
        'even
        '(odd)))

(check= (dfap *dfa-test*) t) 

(definec apply-trans (trans :transition-fun st :state el :element) :state
  (cond ((endp trans) st)
	((== (caar trans) (list st el))
         (cdar trans))
	(t (apply-trans (cdr trans) st el))))

(definec apply-transition (dfa :dfa st :state el :element) :state
  (apply-trans (dfa-trans dfa) st el))

(check= (apply-transition *dfa-test* 'even 1) 'odd)

(definec run-dfa-help (s :state trans :transition-fun w :word) :state
  (if (endp w)
      s
    (run-dfa-help (apply-trans trans s (car w))
		  trans
		  (cdr w))))

(definec run-dfa (m :dfa w :word) :bool
  (b* ((trans  (dfa-trans m))
       (start  (dfa-start m))
       (accept (dfa-accept m)))
    (in (run-dfa-help start trans w) accept)))



;; utility functions
(definec subset (a :tl b :tl) :bool
  (cond ((endp a) t)
	((in (car a) b) (subset (cdr a) b))
	(t nil)))

(check= (subset '(1 1 3) '(1 2 3)) t)

(defdata trans-comps (cons states word))
(definec get-trans-components (trans :transition-fun acc :trans-comps) :trans-comps
  (cond ((endp trans) acc)
	(t (get-trans-components (cdr trans)
				 (cons (cons (caaar trans)
					     (cons (cdar trans)
						   (car acc)))
				       (cons (cadaar trans)
					     (cdr acc)))))))

:q


(load "interface/acl2s-interface.lsp")

(declaim (optimize (safety 3) (speed 0) (space 0) (debug 3)))
(declaim (sb-ext:muffle-conditions style-warning))

(import 'acl2s::(acl2s-compute acl2s-query acl2s-event acl2s-arity
			       dfap apply-transition run-dfa get-trans-components
			       subset))



;;------------------------------------------------------------------------
;; Symbol name manipulation
;;------------------------------------------------------------------------

(defun gen-symb (x n)
  (read-from-string (format nil x n)))

(defun gen-sym-pred (x)
  (gen-symb "~ap" x))

(gen-sym-pred 'dfa)

;;------------------------------------------------------------------------
;; DFA make/reset events generation
;;------------------------------------------------------------------------

(defun reset-history ()
  (acl2s-event `acl2s::(ubt 1)))

(defun reset-dfa-def-fn (d)
  (acl2s-event `acl2s::(ubt ',(gen-sym-pred d))))

(defmacro reset-dfa-def (d)
  `(reset-dfa-def-fn ',d))

(defun mk-dfa-events (name states alphabet start accept transition-fun)
  (let* ((d-state (gen-symb "~a-state" name))
	 (d-states (gen-symb "~a-states" name))
	 (d-elem (gen-symb "~a-element" name))
	 (d-word (gen-symb "~a-word" name))
	 (d-ab (gen-symb "~a-alphabet" name))
	 (d-tdom (gen-symb "~a-t-domain" name))
	 (d-f (gen-symb "~a-transition-function" name)))
  (acl2s-event `acl2s::(defdata ,d-state  (enum (quote ,states))))
  (acl2s-event `acl2s::(defdata ,d-states (listof ,d-state)))
  (acl2s-event `acl2s::(defdata ,d-elem  (enum (quote ,alphabet))))
  (acl2s-event `acl2s::(defdata ,d-word (listof ,d-elem)))
  (acl2s-event `acl2s::(defdata ,d-ab ,d-word))
  (acl2s-event `acl2s::(defdata ,d-tdom (list ,d-state ,d-elem)))
  (acl2s-event `acl2s::(defdata ,d-f (map ,d-tdom ,d-state)))
  (acl2s-event `acl2s::(defdata ,name (list ,d-states ,d-ab ,d-f ,d-state ,d-states)))))
  

(defun gen-dfa-fn (&key name states alphabet start accept transition-fun)
  (let* ((isdfatype (dfap `acl2s::(,states ,alphabet ,transition-fun ,start ,accept))))
    (if (not isdfatype)
	(error "Check if input is a DFA")
      (mk-dfa-events name states alphabet start accept transition-fun))))


(defmacro gen-dfa (&key name states alphabet start accept transition-fun)
  (unless name (error "name missing"))
  (unless states (error "states missing"))
  (unless alphabet (error "alphabet missing"))
  (unless start (error "start missing"))
  (unless accept (error "accept missing"))
  (unless transition-fun (error "transition-fun missing"))
  `(gen-dfa-fn :name ',name
	       :states ',states
	       :alphabet ',alphabet
	       :start ',start
	       :accept ',accept
	       :transition-fun ',transition-fun))

;;------------------------------------------------------------------------
;; DFA check events generation
;;------------------------------------------------------------------------

(defun check-dfa-equiv-fn (d1-name d2-name)
  (let* ((dnp (gen-symb "~a-wordp" d1-name)))
    (acl2s-event `acl2s::(test? (=> (,dnp w)
				    (== (run-dfa ,d1-name w)
					(run-dfa ,d2-name w)))))))

(defmacro check-dfa-equiv (d1-name d2-name)
  (unless d2-name (error "require two input dfas to check equivalence"))
  `(check-dfa-equiv-fn ',d1-name ',d2-name))



(defun equivalent-states-fn (d1-name d1-states)
  (loop for s in d1-states
	collect (acl2s-event `acl2s::(test? (=> (,(gen-symb "~a-wordp" d1-name) w)
			    (!= (run-dfa-help (dfa-start ,d1-name)
					      (dfa-trans ,d1-name) w)
				',s))))))

(defmacro equivalent-states (d1-states d2-states)
  (unless d2-states (error "require two input dfa states to check equivalence"))
  `(equivalent-states-fn ',d1-states ',d2-states))


(equivalent-states dfa1 (odd even))


(test? (=> (and (dfa1-wordp w) (dfa1p d))
	   (!= (run-dfa-help (dfa-start d) (dfa-trans d) w)
               'odd)))



(gen-dfa
 :name           dfa1
 :states         (even odd)
 :alphabet       (0 1)
 :start          even
 :accept         (odd)
 :transition-fun (((even 0) . even)
		  ((even 1) . odd)
		  ((odd  0) . odd)
		  ((odd  1) . even)))

(gen-dfa
 :name           dfa2-buggy
 :states         (even odd)
 :alphabet       (0 1)
 :start          even
 :accept         (odd)
 :transition-fun (((even 0) . odd)
		  ((even 1) . odd)
		  ((odd  0) . odd)
		  ((odd  1) . odd)))



(check-dfa-equiv dfa1 dfa2-buggy)







**function to prove equivalence




(dfap (list '(even odd)
	    '(0 1)
	    '(((even 0) . even)
	      ((even 1) . odd)
	      ((odd  0) . odd)
	      ((odd  1) . even))
	    'even
	    '(odd)))
(lp)

; Generated

(defdata d1-state          (enum '(odd even)))
(defdata d1-states         (listof d1-state))
(defdata d1-element        (enum '(0 1)))
(defdata d1-word           (listof d1-element)) 
(defdata d1-alphabet       d1-word)
(defdata d1-t-domain       (list d1-state d1-element))
(defdata d1-transition-fun (map d1-t-domain d1-state))

(defdata d1-dfa (list d1-states d1-alphabet d1-transition-fun d1-state d1-states))

(defdata-subtype-strict d1-state state)
(defdata-subtype-strict d1-states states)
(defdata-subtype-strict d1-element element)
(defdata-subtype-strict d1-word word)
(defdata-subtype-strict d1-t-domain t-domain)
(defdata-subtype-strict d1-transition-fun transition-fun)
(defdata-subtype-strict d1-dfa dfa)

(defthm dfa-states-d1
  (=> (d1-dfap d) (d1-statesp (dfa-states d))))

(defthm dfa-alphabet-d1
  (=> (d1-dfap d) (d1-alphabetp (dfa-alphabet d))))

(defthm dfa-trans-d1
  (=> (d1-dfap d) (d1-transition-funp (dfa-trans d))))

(defthm dfa-start-d1
  (=> (d1-dfap d) (d1-statep (dfa-start d))))

(defthm dfa-accept-d1
  (=> (d1-dfap d) (d1-statesp (dfa-accept d))))

(defthm d1-trans-rip-lemma
  (=> (d1-transition-funp (cons (cons (list s el) x) y))
      (d1-statep x))
  :rule-classes ((:rewrite :match-free :all)
                 (:forward-chaining)))

(in-theory (disable d1-statep
                    d1-t-domainp
                    d1-elementp
                    d1-transition-funp
                    ))
           
(defthm apply-trans-d1
  (=> (^ (d1-transition-funp trans)
         (d1-statep s)
         (d1-elementp el))
      (d1-statep (apply-trans trans s el))))

(defthm run-dfa-help-d1
  (=> (^ (d1-statep s)
         (d1-transition-funp trans)
         (d1-wordp w))
      (d1-statep (run-dfa-help s trans w))))

(defconst *dfa1*
  (list '(even odd)
        '(0 1)
        '(((even 0) . even)
	  ((even 1) . odd)
	  ((odd  0) . odd)
	  ((odd  1) . even))
        'even
        '(odd)))

(check= (dfap *dfa1*) t) 
(check= (d1-dfap *dfa1*) t) 

(run-dfa *dfa1* '(1 1 0 0 1 0 1 1))
(run-dfa *dfa1* '(1 1 0 0 1 0 1 1 1))

;; generating words with odd ones, using counter example generator~
(test? (=> (d1-wordp w)
	   (not (run-dfa *dfa1* w))))

; A buggy version 
(defconst *dfa2*
  (list '(even odd)
        '(0 1)
        '(((even 0) . odd)
	  ((even 1) . odd)
	  ((odd  0) . even)
	  ((odd  1) . odd))
        'even
        '(odd)))


;; Comparing two different dfas
(test? (=> (and (d1-wordp w)
		(> (len w) 5))
	   (== (run-dfa *dfa1* w)
               (run-dfa *dfa2* w))))

(defdata d3-state       (enum '(e1 e2 o1 o2)))
(defdata d3-states      (listof d3-state))
(defdata d3-element     (enum '(0 1)) :do-not-alias t)
(defdata d3-word        (listof d3-element)) 
(defdata d3-alphabet    d3-word)
(defdata d3-t-domain    (list d3-state d3-element))
(defdata d3-transition-fun (map d3-t-domain d3-state))

(defdata d3-dfa (list d3-states d3-alphabet d3-transition-fun d3-state d3-states))

(defdata-subtype-strict d3-state state)
(defdata-subtype-strict d3-states states)
(defdata-subtype-strict d3-element element)
(defdata-subtype-strict d3-word word)
(defdata-subtype-strict d3-t-domain t-domain)
(defdata-subtype-strict d3-transition-fun transition-fun)
(defdata-subtype-strict d3-dfa dfa)

(defdata-equal d1-element d3-element
  :hints (("goal" :in-theory (enable d1-elementp))))

(defdata-equal d1-word d3-word)

(defthm dfa-states-d3
  (=> (d3-dfap d) (d3-statesp (dfa-states d))))

(defthm dfa-alphabet-d3
  (=> (d3-dfap d) (d3-alphabetp (dfa-alphabet d))))

(defthm dfa-trans-d3
  (=> (d3-dfap d) (d3-transition-funp (dfa-trans d))))

(defthm dfa-start-d3
  (=> (d3-dfap d) (d3-statep (dfa-start d))))

(defthm dfa-accept-d3
  (=> (d3-dfap d) (d3-statesp (dfa-accept d))))

(defthm d3-trans-rip-lemma
  (=> (d3-transition-funp (cons (cons (list s el) x) y))
      (d3-statep x)))
 
(in-theory (disable ;statep
                    d3-statep
                    ;t-domainp
                    d3-t-domainp
                    ;elementp
                    d3-elementp
                    ;transition-funp
                    d3-transition-funp
                    ;dfa-trans
                    ;dfa-start
                    ;dfa-accept
))
           
(defthm apply-trans-d3
  (=> (^ (d3-transition-funp trans)
         (d3-statep s)
         (d3-elementp el))
      (d3-statep (apply-trans trans s el))))

(defthm run-dfa-help-d3
  (=> (^ (d3-statep s)
         (d3-transition-funp trans)
         (d3-wordp w))
      (d3-statep (run-dfa-help s trans w))))

                   
; An alternate definition
(defconst *dfa8*
  (list '(e1 e2 e3 o1 o2 o3)
        '(0 1)
        '(((e1 0) . e1)
	  ((e1 1) . o1)
          ((e2 0) . e2)
	  ((e2 1) . o2)
	  ((o1 0) . o2)
	  ((o1 1) . e2)
	  ((o2 0) . o2)
	  ((o2 1) . o2))
        'e1
        '(o1 o2)))

;; Comparing two equivalent dfas
(test? (=> (wordp w)
	   (== (run-dfa *dfa1* w)
               (run-dfa *dfa8* w))))

;; Comparing two equivalent dfas
(test? (=> (w1p w)
	   (== (run-dfa *dfa1* w)
               (run-dfa *dfa8* w))))



;; Comparing two equivalent dfas
(test? (=> (and (w1p w)
		(> (len w) 5))
	   (== (run-dfa *dfa1* w)
               (run-dfa *dfa6* w))))







#|
(thm (=> (d1-wordp w)
	 (== (run-dfa *dfa1* w)
             (run-dfa *dfa3* w))))
|#

; Here is the idea for automatically generating the lemmas to prove
; the equivalence of two DFAs. I'm not claiming that this is always
; going to work, but if the first DFA, say the one that the TA defined
; is minimal, then I think this will work, conceptually.
;
; The idea is to use test? to find words that reach every state in
; both DFAs and to then create equivalence classes relating states in
; one DFA with states in the other. So, run all the words you find on
; both DFAs and the states you reach are put in the same class. So,
; here are the test? queries. Actually if the first DFA is minimal,
; you only need to do this with the second DFA.

(test? (=> (d1-wordp w)
	   (!= (run-dfa-help (dfa-start *dfa1*) (dfa-trans *dfa1*) w)
               'even)))

; So even and e1 are the same. Not needed since DFA1 is minimal.

(test? (=> (d1-wordp w)
	   (!= (run-dfa-help (dfa-start *dfa1*) (dfa-trans *dfa1*) w)
               'odd)))

; The queries below are the ones I really need.

(test? (=> (d1-wordp w)
	   (!= (run-dfa-help (dfa-start *dfa3*) (dfa-trans *dfa3*) w)
               'e1)))

(test? (=> (d1-wordp w)
	   (!= (run-dfa-help (dfa-start *dfa3*) (dfa-trans *dfa3*) w)
               'e2)))

(test? (=> (d1-wordp w)
	   (!= (run-dfa-help (dfa-start *dfa3*) (dfa-trans *dfa3*) w)
               'o1)))

(test? (=> (d1-wordp w)
	   (!= (run-dfa-help (dfa-start *dfa3*) (dfa-trans *dfa3*) w)
               'o2)))

; Now I can defined the equivalence relation. If D1 is minimal can
; simplify eg instead of (^ (in d1-st '(even)) ...) we can write
; (^ (== d1-st 'even) ....)
(definec d1-d3-equiv (d1-st :d1-state d3-st :d3-state) :bool
  (v (^ (in d1-st '(even)) (in d3-st '(e1 e2)))
     (^ (in d1-st '(odd)) (in d3-st '(o1 o2)))))

; Now we prove that apply-trans preserves equivalence.

#|
; We don't need this defthm since it does not require induction, but
; it may help by providing counterexamples. So I am commenting out
; here.


(defthm d1-d3-equiv-lemma
  (=> (^ (d1-elementp el)
         (d1-statep d1-st)
         (d3-statep d3-st)
         (d1-d3-equiv d1-st d3-st))
      (d1-d3-equiv (apply-trans (dfa-trans *dfa1*) d1-st el)
                   (apply-trans (dfa-trans *dfa3*) d3-st el))))

(in-theory (disable dfa-trans))
|#

(defthm d1-d3-equiv-lemma2
  (=> (^ (d1-wordp w)
         (d1-statep d1-st)
         (d3-statep d3-st)
         (d1-d3-equiv d1-st d3-st))
      (d1-d3-equiv (run-dfa-help d1-st (dfa-trans *dfa1*) w)
                   (run-dfa-help d3-st (dfa-trans *dfa3*) w))))

(thm (=> (d1-wordp w)
	 (== (run-dfa *dfa1* w)
             (run-dfa *dfa3* w)))
  :hints (("goal" :use ((:instance d1-d3-equiv-lemma2
                                   (d1-st (dfa-start *dfa1*))
                                   (d3-st (dfa-start *dfa3*)))))))
