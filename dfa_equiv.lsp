(include-book "dfa")
:q
(load "dfa_raw_code.lsp")
(in-package "ACL2S")

(defun main (flist)
  (quiet-mode-on)
  (check-dfa-equivalence (car flist) (second flist))
  (terpri)
  (sb-ext:exit))

(save-exec "dfa_equiv_exec" nil
           :init-forms '((set-gag-mode nil)
                         (value :q))
           :toplevel-args "--eval '(declaim (sb-ext:muffle-conditions style-warning))' --eval '(acl2s::main (cdr sb-ext:*posix-argv*))' --disable-debugger")

