(include-book "dfa")

:q
(in-package "ACL2S")
(declaim (optimize (safety 3) (speed 0) (space 0) (debug 3)))
(declaim (sb-ext:muffle-conditions style-warning))

;;------------------------------------------------------------------------
;; Symbol name manipulation
;;------------------------------------------------------------------------

(defun gen-symb (x n)
  (read-from-string (format nil x n)))

(defun gen-sym-pred (x)
  (gen-symb "~ap" x))

(gen-sym-pred 'dfa)

;;------------------------------------------------------------------------
;; DFA make/reset events generation
;;------------------------------------------------------------------------

(defun reset-history ()
  (acl2s-event `acl2s::(ubt 1)))

(defun reset-dfa-def (d)
  (acl2s-event `acl2s::(ubt ',(gen-sym-pred d))))

(defun error-and-reset (msg def)
  (progn (reset-dfa-def def)
	 (error (format nil "[~a]" msg))
         (sb-ext:exit)))


;; generates defdata events while also checking if input is actually a DFA
(defun mk-dfa-events (name states alphabet start accept transition-fun)
  (let* ((d-state (gen-symb "~a-state" name))
	 (d-states (gen-symb "~a-states" name))
	 (d-elem (gen-symb "~a-element" name))
	 (d-word (gen-symb "~a-word" name))
	 (d-ab (gen-symb "~a-alphabet" name))
	 (d-tdom (gen-symb "~a-t-domain" name))
	 (d-f (gen-symb "~a-transition-function" name))
	 (d-fp (gen-sym-pred d-f)))
    (acl2s-event `acl2s::(defdata ,d-state  (enum (quote ,states))))
    (unless (statesp `acl2s::,states) (error-and-reset "incorrect states" d-state))
    (acl2s-event `acl2s::(defdata ,d-states (listof ,d-state)))
    (acl2s-event `acl2s::(defdata ,d-elem  (enum (quote ,alphabet))))
    (acl2s-event `acl2s::(defdata ,d-word (listof ,d-elem)))
    (acl2s-event `acl2s::(defdata ,d-ab ,d-word))
    (unless (alphabetp `acl2s::,alphabet) (error-and-reset "incorrect alphabet" d-state))
    (unless (in start `acl2s::,states) (error-and-reset "incorrect start state" d-state))
    (unless (subset `acl2s::,accept `acl2s::,states)
      (error-and-reset "incorrect accept states" d-state))
    (acl2s-event `acl2s::(defdata ,d-tdom (list ,d-state ,d-elem)))
    (acl2s-event `acl2s::(defdata ,d-f (map ,d-tdom ,d-state)))
    (unless (second (acl2s-compute `acl2s::(,d-fp (quote ,transition-fun))))
       (error-and-reset "incorrect transition function" d-state))
    (acl2s-event `acl2s::(defdata ,name (list ,d-states ,d-ab ,d-f ,d-state ,d-states)))
    (unless (dfap `acl2s::(,states ,alphabet ,transition-fun ,start ,accept))
	    (error-and-reset "incorrect dfa" d-state))))



(defun gen-dfa-fn (&key name states alphabet start accept transition-fun)
 (let* ((df `acl2s::(,states ,alphabet ,transition-fun ,start ,accept)))
    (mk-dfa-events name states alphabet start accept transition-fun)
    (cons name df)))



(defmacro gen-dfa (&key name states alphabet start accept transition-fun)
  (unless name (error "name missing"))
  (unless states (error "states missing"))
  (unless alphabet (error "alphabet missing"))
  (unless start (error "start missing"))
  (unless accept (error "accept missing"))
  (unless transition-fun (error "transition-fun missing"))
  `(gen-dfa-fn :name ',name
	       :states ',states
	       :alphabet ',alphabet
	       :start ',start
	       :accept ',accept
	       :transition-fun ',transition-fun))
  

;;------------------------------------------------------------------------
;; DFA check events generation
;;------------------------------------------------------------------------

;Returns alist of state x [word]
(defun state-word-map (dfa1-name dfa1)
  (let* ((dn (gen-symb "~a-wordp" dfa1-name))
	 (ds (car dfa1))
	 (wmap (loop for s in ds
		     collect
		     (cons s
			   (itest?-query
			    `acl2s::(=> (,dn w)
					(!= (run-dfa-help (dfa-start ',dfa1)
							  (dfa-trans ',dfa1) w)
					    ',s)))))))
    (loop for tup in wmap
	  collect (cons (car tup)
			(mapcar (lambda (x) (second (car x))) (third tup))))))



(defun state-state-map (dfa1-st-wrd-map dfa1 dfa2)
  (loop for dfa1-st-wrds in dfa1-st-wrd-map
	collect  (cons (car dfa1-st-wrds) (if (consp (cdr dfa1-st-wrds))
		     (remove-duplicates (mapcar (lambda (w)
			       (second (acl2s-compute
					`acl2s::(run-dfa-help (dfa-start ',dfa2)
							      (dfa-trans ',dfa2) ,w)))) (cdr dfa1-st-wrds)))
		   nil))))

(defun expand-map (m)
  (cond ((endp m) nil)
	(t (append (mapcar (lambda (x) (list (caar m) x)) (cdar m))
		   (expand-map (cdr m))))))


(defun multimaps (a m)
  (let* ((st1s (remove-duplicates
	   (mapcar 'second
		   (remove-if-not
		    #'(lambda (p) (equal (first a) (first p))) m)))))
    (if (> (length st1s) 1)
	st1s
      nil)))


(defun nequiv (m)
    (remove-if-not
     #'(lambda (l) (> (length l) 1))
     (mapcar (lambda (p) (cons (car p) (multimaps p m))) m)))


(defun query-equivalence (dfa1-name dfa1 dfa2-name dfa2 d2-d1-st-map st1-wd-map)
  (let* ((dn (gen-symb "~a-wordp" dfa1-name))
	 (d2-state (gen-symb "~a-state" dfa2-name))
	 (emap (expand-map d2-d1-st-map))
	 (eqv  (nequiv emap)))
    ;;(print emap)
    (if (endp eqv)
	(itest?-query
	 `acl2s::(=> (,dn w)
		     (not (in ',emap
			      (list
			       (run-dfa-help (dfa-start ',dfa1)
					     (dfa-trans ',dfa1) w)
			       (run-dfa-help (dfa-start ',dfa2)
					     (dfa-trans ',dfa2) w))))))
      (let* ((st1s (cdar eqv)))
	(reset-dfa-def (gen-symb "~a-state" dfa2-name))
	(cons t (mapcar (lambda (x) (cdr (assoc x st1-wd-map))) st1s))))))

;; f1 is instructor file (minimal and correct).
;; f2 is student file (may be buggy)
(defun check-dfa-input-run (f1 f2 w)
  (let* ((dfa1 (eval (with-open-file (infile f1) (read infile))))
	 (dfa2 (eval (with-open-file (infile f2) (read infile))))
	 (dfa2st (gen-symb "~a-state" (first dfa2))))
    ;; Check if student is using same alphabet
    (unless (and (subset (third dfa1) (third dfa2))
		 (subset (third dfa2) (third dfa1)))
      (error-and-reset "incorrect alphabet provided"
		       (gen-symb "~a-state" (first dfa2))))
    ;;we find state-word maps of dfa2, then map those states to dfa1 (assumed minimal)
    (if (equal (run-dfa (cdr dfa1) w)
	       (run-dfa (cdr dfa2) w))
	(print (format t "[Passed test case]"))
      (print (format t "[Failed test case]")))
    ))

;; f1 is instructor file (minimal and correct).
;; f2 is student file (may be buggy)
(defun check-dfa-equivalence (f1 f2)
  (let* ((dfa1 (eval (with-open-file (infile f1) (read infile))))
	 (dfa2 (eval (with-open-file (infile f2) (read infile))))
	 (dfa2st (gen-symb "~a-state" (first dfa2))))
    ;; Check if student is using same alphabet
    (unless (and (subset (third dfa1) (third dfa2))
		 (subset (third dfa2) (third dfa1)))
      (error-and-reset "incorrect alphabet provided"
		       (gen-symb "~a-state" (first dfa2))))
    ;;we find state-word maps of dfa2, then map those states to dfa1 (assumed minimal)
    
    (let* ((st2-wd-map (state-word-map (first dfa2) (cdr dfa2)))
	   (st1-wd-map (state-word-map (first dfa1) (cdr dfa1)))
	   (st2-st1-map (state-state-map st2-wd-map (cdr dfa2) (cdr dfa1)))
	   (res (query-equivalence (car dfa1) (cdr dfa1) (car dfa2 )(cdr dfa2) st2-st1-map st1-wd-map)))
      (reset-dfa-def dfa2st)
      (if (car res)
	  (print (format t "[Following words do not lead to the correct state :~a]" (cdr res)))
	(print (format t "[Passed equivalence check!]")))
      )))
