* <2021-10-09 Sat>
Here are some further notes above and beyond what we discussed in our
meeting today.
** Intro
   - ATOCA provides a computational view of automata which allows one
     to reason about their constructions using properties that can be
     described in a programming language.
     - The inference doesn't make sense.
     - There are two points here:
       - you get an executable version of the automata you define
         (computational view is not super clear; of course automata
         are computational; it is the theory of computation class) and
         what is view, exactly? the point is that you get executable
         automata.
       - you also get reasoning ability using properties, which is
         independent of executability
       - so clarify
     - Furthermore, access to a powerful theorem prover allows an
       instructor to test their constructions, prove properties or
       even add a new theory or model with minimal effort.
       - clarify what minimal effort means here; what do they have to
         do? If you bring this up in the intro, make sure it gets
         discussed later also; otherwise, remove the minimal effort
         part; just the ability to do this is useful.
** Related Work
   - Automata Tu- tor has been used to teach thousands of students at
     over 30 universities, with high reported satisfaction among both
     students and instructors.
     - Reference?
   - Not at all clear why one would choose atoca vs automat tutor. Say
     something about that
   - word that one of the DFAs accepts but the other rejects) with
     worst-case runtime complexity “nearly linear” in the total number
     of states
     - give the actual complexity. why are you referencing 16 here for
       a vague characterization. reference a book that has the
       algorithm and its complexity or the original source
   - While it would be possible to integrate DFA equivalence decision
     procedures into ATOCA, we decided to go with a uniform approach
     that works similarly across DFAs, PDAs and TMs.
     - not compellling; maybe put it in future work
** Illustrative Example
   - We mentioned that you could point out that defdata is used to
     prove equivalence of alphabet, but since this is just a finite
     list corresponding to a set, equivalence is a trivial matter, so
     don't make a big deal of it
   - Starting domain (Q1 nil nil) missing or doesn’t add base stack
     symbol.
     - I don't understand this feedback.
   - Checks for transition function however differ.
     - Awkward. Clarify
   - This is because the values for missing domain entries
     are assumed as empty sets.
     - What? What's a missing domain entry? Explain in reasonable
       English. Make sure the rest of the paragraph makes sense.
   - Starting domain (Q1 nil nil) missing or doesn’t add base stack
     symbol.
     - Make this human understandable
   - The transition function of a PDA is not required to be
     complete. This is because the values for missing domain entries
     are assumed as empty sets.
     - Again, domain entry?
   - Your pda is incorrect.
     - Did you check it using the tool?
       - If so, did the tool fail to catch this?
       - If not, why not?
       - I looked in the repo and did find a similar example in
         pda.lisp, but I already told you to make sure you have all
         the examples in the paper formalized and checked. I shouldn't
         have to do this checking at this point in time.
     - The transition (q2 nil nil) is incorrect. You should only
       transition to q3 if you see a z.
   - In the checking TMs section, there is no example of a failing
     test. The plan was for you to have such an example. As is, the
     section seems out of place.
   - Looking at pda.lisp, the file seems to be a mess.
     - There are skip-proofs that don't seem necessary.
     - You define run-pda to only run for 20 steps!
     - I tried the following simple experiment, which led to a stack
       overflow. All I did was to add one transition to the pda from
       the paper.
       #+BEGIN_SRC lisp
         (gen-pda
          :name pda1
          :states (q0 q1 q2 q3)
          :alphabet (0 1)
          :stack-alphabet (0 b)
          :start-state q0
          :accept-states (q0 q3)
          :transition-fun (((q0 nil nil) . ((q1 b)))
                           ((q1 nil 0) . ((q1 0)))
                           ((q1 0   nil) . ((q1 0)))
                           ((q1 1   0)   . ((q2 nil)))
                           ((q2 1   0)   . ((q2 nil)))
                           ((q2 nil b)   . ((q3 nil)))))

         (run-pda *pda1* '(0 0 0 0 0 0 0 1 1 1 1 1 1 1))
       #+END_SRC
   - I'm sorry, but I'm out. I'm not putting my name on the paper. If
     you guys want to submit the paper, feel free, but, at this point,
     I don't have any confidence that this is a useable tool.
   

