import unittest
from gradescope_utils.autograder_utils.decorators import weight
import subprocess32 as subprocess
import re

def extract_out(output):
    mymatch = re.search(r'\[(.+)\]', str(output))
    out = mymatch.group(0).replace("\\n","").replace("\\'","")
    out2 = re.sub(' +', ' ',out)
    out = out2.replace(")",")\n")
    print(out)
    return out

class TestSimpleArithmetic(unittest.TestCase):
    
    @weight(5)
    def test_accept0(self):
        """Test accept"""
        calc = subprocess.Popen(['./tm_run_exec', 'tm2.txt', '',''], stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        output = calc.communicate()[0]
        out = extract_out(output)
        self.assertTrue("ACCEPTED" in out)  # Check for presence of accepted
        calc.terminate()
        
    @weight(1)
    def test_accept1(self):
        """Test accept"""
        calc = subprocess.Popen(['./tm_run_exec', 'tm2.txt', '11y1y','11111'], stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        output = calc.communicate()[0]
        out = extract_out(output)
        self.assertTrue("Passed" in out)  # Check for presence of accepted
        calc.terminate()

    @weight(1)
    def test_accept2(self):
        """Test accept"""
        calc = subprocess.Popen(['./tm_run_exec', 'tm2.txt', 'y101y1x','1101110'], stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        output = calc.communicate()[0]
        out = extract_out(output)
        self.assertTrue("Passed" in out)  # Check for presence of accepted
        calc.terminate()

    @weight(1)
    def test_accept3(self):
        """Test accept"""
        calc = subprocess.Popen(['./tm_run_exec', 'tm2.txt', 'y1y','111'], stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        output = calc.communicate()[0]
        out = extract_out(output)
        self.assertTrue("Passed" in out)  # Check for presence of accepted
        calc.terminate()

    @weight(1)
    def test_accept4(self):
        """Test accept"""
        calc = subprocess.Popen(['./tm_run_exec', 'tm2.txt', 'x101x0x1','01010001'], stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        output = calc.communicate()[0]
        out = extract_out(output)
        self.assertTrue("Passed" in out)  # Check for presence of accepted
        calc.terminate()

    @weight(1)
    def test_accept5(self):
        """Test accept"""
        calc = subprocess.Popen(['./tm_run_exec', 'tm2.txt', 'x0y1','0011'], stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        output = calc.communicate()[0]
        out = extract_out(output)
        self.assertTrue("Passed" in out)  # Check for presence of accepted
        calc.terminate()
