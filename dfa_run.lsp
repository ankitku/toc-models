(include-book "dfa")
:q
(load "dfa_raw_code.lsp")
(in-package "ACL2S")

; (with-input-from-string (in (cddr flist))
;                                     (loop for x = (read in nil nil)
;                                           while x collect x))

(defun main (flist)
  (quiet-mode-on)
  (let* ((w (mapcar 'parse-integer (cddr flist))))
    (check-dfa-input-run (first flist) (second flist) w)
    (terpri)
    (sb-ext:exit)))
    
(save-exec "dfa_run_exec" nil
           :init-forms '((set-gag-mode nil)
                         (value :q))
           :toplevel-args "--eval '(declaim (sb-ext:muffle-conditions style-warning))' --eval '(acl2s::main (cdr sb-ext:*posix-argv*))' --disable-debugger")


