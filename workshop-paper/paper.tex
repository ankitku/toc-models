\documentclass{easychair}
\usepackage{preamble}

\begin{document}
\title{Automated Grading of Automata with ACL2s}
\author{Ankit Kumar, Andrew Walter \and Panagiotis Manolios}
\institute{Northeastern University\\
\email{\{ankitk,atwalter,pete\}@ccs.neu.edu}}

\authorrunning{Kumar, Walter and Manolios}
\titlerunning{Automated Grading of Automata with {ACL2s}}
\maketitle

\begin{abstract}
  Almost all Computer Science programs require students to take a
  course on the Theory of Computation (ToC) which covers various
  models of computation such as finite automata, push-down automata
  and Turing machines. ToC courses tend to emphasize paper-and-pencil
  mathematics over programming. As a consequence, students typically
  receive feedback on their work only after it has been graded, which
  can take several weeks. We present tools that provide automatic
  feedback for constructing finite automata, push-down automata and
  Turing machines. These tools are based on the ACL2s interactive
  theorem prover, which provides advanced methods for stating as well
  as proving and disproving conjectures. Since feedback is automatic,
  our tools can be deployed at scale and integrated into massively
  open online courses.
\end{abstract}

\section{Introduction}

In Theory of Computation (ToC) courses, students study models of
computation, including Deterministic Finite Automata (DFAs), Push-Down
Automata (PDAs) and Turing machines (TMs). When constructing automata,
students benefit from getting immediate, automatic feedback.  We
present tools based on the ACL2s theorem prover~\cite{acl2s11} for
this purpose. Our tools provide convenient forms for defining,
executing, testing and reasoning about automata. Our tools have been
integrated into Gradescope~\cite{gradescope}, a widely-used online
grading system. Students upload their automata, which are checked for
consistency and semantic equivalence with instructor-provided
solutions. Students receive immediate feedback, \eg, if their solutions
are incorrect, they are provided with inputs which their automata
incorrectly classify. Students are then able to update and resubmit
their automata. Our tools are publicly available~\cite{tocmodels}.

\section{Illustrative Example}
Consider a homework problem to construct a DFA that can recognize
words in $\{0,1\}^*$ consisting of an even number of ones. A DFA $M$
is a 5-tuple, $(Q, \Sigma, \delta, q_0, F)$ where $Q$ is a finite set
of states, $\Sigma$ is an alphabet, $\delta : Q \times \Sigma
\rightarrow Q$ is a transition function, $q_0 \in Q$ is a start state
and $F \subseteq Q$ is a finite set of accept states. Suppose a
student enters an ACL2s form as shown:

\lstset{language=}
\begin{lstlisting}
  (gen-dfa
   :name     student-dfa
   :states   (e1 e2 o1 o2)
   :alphabet (0)
   :start    e1
   :accept   (o1 o2)
   :transition-fun ((e1 0 e1) (e1 2 o1) (e2 0 e2) (e2 2 e2)
                    (o1 0 o2) (o1 2 e2) (o2 0 o1) (o2 2 e1)))
\end{lstlisting}

The \gendfa\ form above defines a DFA with the name \sdfa.  The
order of components appearing in the form does not matter, \eg,
swapping the order of \st\ and \ac\ yields the same DFA. On reading a
\gendfa\ form, our tool performs several checks, including (1)
checking that the name is new (2) that all components of a DFA are
provided and (3) checking the consistency of each component, \eg, that
the transition function is a total function from the appropriate
domain to the appropriate co-domain.

Given the form above, our tool reports that \tf\ ($\delta$) is not a
function with domain $ Q \times \Sigma $. This is because
\ab\ ($\Sigma$) specified in the form consists only of \zero, whereas
the alphabet in \codify{:transition-fun} consists of both \zero\ and
\two. After receiving this feedback, the student can update \ab\ as
follows.
\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
  :alphabet       (0 2)
\end{lstlisting}
The updated form passes all of our checks, indicating that the
student's submitted form represents a valid DFA. What remains is to
check whether this construction is correct. The specification for a
correct automaton is a solution provided by the instructor:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
 (gen-dfa
  :name           instructor-dfa
  :states         (even odd)
  :alphabet       (0 1)
  :start          even
  :accept         (odd)
  :transition-fun ((even 0 even) (even 1 odd) (odd 0 odd) (odd 1 even)))
\end{lstlisting}

Testing for correctness reduces to testing equivalence between the
student's solution \sdfa\ and the instructor's solution \idfa.
The instructor's DFA consists of two states \even\ and \odd.
When checking for equivalence, our tool responds:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
Incorrect alphabet provided.
\end{lstlisting}

This is because the student's alphabet does not match with the one
provided in the instructor's DFA. The student corrects their alphabet
and transition function.

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
  :alphabet (0 1)
  :transition-fun ((e1 0 e1) (e1 1 o1) (e2 0 e2) (e2 1 e2)
                   (o1 0 o2) (o1 1 e2) (o2 0 o1) (o2 1 e1)))
\end{lstlisting}

But \sdfa\ is still incorrect, as is pointed out by our tool in the
following feedback:
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
Transition function error. The following words are misclassified:
('(0 1 1 1) '(1 1 1 0) '(1 1 1))
\end{lstlisting}

Each list in the feedback represents a word that is misclassified by
\sdfa. With this feedback, students learn something new about their
construction and can run their DFA on the generated words as shown:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
(run-dfa student-dfa (1 1 1))
\end{lstlisting}
The \codify{run-dfa} form runs \sdfa\ on the input word
\codify{(1 1 1)} generating the following output:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
E2
\end{lstlisting}

This helps the student realize that any transition from \et\ leads
back to \et. The student corrects this mistake as shown:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
 :transition-fun ((e1 0  e1) (e1 1  o1) (e2 0  e2) (e2 1  o2)
	          (o1 0  o2) (o1 1  e2) (o2 0  o1) (o2 1  e1))
\end{lstlisting}

The updated solution is finally accepted by the autograder, which outputs:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
 student-dfa is correct. 
\end{lstlisting}

\section{Tool Architecture}
As already mentioned, our tool is based on the ACL2s theorem
prover~\cite{acl2s11,acl2swebpage}, an extension of
ACL2~\cite{car,acl2webpage} which consists of a programming language,
a logic for the language and an interactive theorem prover.

\subsection{Solution format}
We have well defined input formats for specifying automata.  A
solution file consists of one or more \genx\ forms where $x$ may be
either one of \codify{dfa}, \codify{pda} or \codify{tm}. Each such
form provides a declarative description of its corresponding
automaton. The description is faithful to its textbook
definition~\cite{tocbook}. Hence the input format can be naturally
explained and is easy to understand.

\subsection{Validating automata}
Our tool validates a \genx\ form by checking whether
\begin{itemize}
\item all required components of the automaton are provided,
\item the start state is one of the given states,
\item the set of accept states is a subset of the set of states,
\item the domain of the transition function is of the right type,
\item the co-domain of the transition function is of the right type,
\item additional model-specific checks for each of PDAs and TMs, \eg,
  the blank tape symbol does not appear in the alphabet of a TM. 
\end{itemize}

\subsection{Evaluating student constructions}
In the context of the ACL2s theorem prover, an automaton description
is converted into corresponding definitions of states, alphabets,
transition-functions and functions that run the automata on input
words. An instructor can use these definitions to evaluate automata in
the following ways:

\textbf{Testing equivalence:} To test if two automata are
  equivalent, we first check if their alphabets match:

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
  (defdata-equal instructor-dfa-alphabet student-dfa-alphabet)
\end{lstlisting}

Here \codify{instructor-dfa-alphabet} and
\codify{student-dfa-alphabet} are definitions generated by
\gendfa\ for the alphabet of \codify{instructor-dfa} and
\sdfa\ respectively.  If the alphabets differ, counter-examples are
generated.  To complete the check of equivalence, we check the
following property.

\lstset{language=}
\begin{lstlisting}[belowskip=-0.2 \baselineskip]
  (test? (=> (instructor-dfa-wordp w)
             (equal (accept-dfa student-dfa w)
                    (accept-dfa instructor-dfa w))))
\end{lstlisting}

\codify{(accept-dfa student-dfa w)} checks whether \codify{(run-dfa
  student-dfa w)} returns an accept state of \sdfa. Any
counter-example generated by this \codify{test?} form indicates a word
accepted by exactly one of the constructions.

\textbf{Testing properties:} Since checking the equivalence of PDAs and
  TMs is undecidable, it is useful for the instructor to have a
  collection of tests that can check for properties of a student's
  construction. For example, in context of our running example,

  \lstset{language=}
  \begin{lstlisting}[belowskip=-0.2 \baselineskip]
    (test? (=> (instructor-dfa-wordp w)
               (accept-dfa student-dfa (append w w))))
  \end{lstlisting}
         
  checks that \sdfa\ accepts words of the form
  \codify{ww}. This property holds for DFAs that accept words
  containing an even number of \codify{1}s. Such properties represent an
  infinite class of test cases a submission can be evaluated against.
  Of course, ground expressions such as unit tests can be similarly 
  defined.

\section{Autograding Assignments}

Our tool can be packaged as an executable and deployed on any online
grading platform that can run the executable on student submissions
and can use the output generated by our tool to grade assignments.

\subsection{Packaging an executable}
ACL2s provides a feature that allows a user to save the state of a
running ACL2s process as an executable file. In the case of our tool,
the state includes instructor versions of each automata being checked
as well as forms required for testing student constructions.

\subsection{Autograding assignments on Gradescope}
Since there is no support for the ACL2s language in Gradescope, we
need to package our tool in a custom Docker image~\cite{acl2simage},
which we then provide to Gradescope. For grading student submissions,
we have a Python program that checks the output generated by the tool
when it is run on a student submission. Students have access to the
generated output and can submit repeatedly until they get their
construction right.

\section{Effectiveness}
We deployed our tool on Gradescope~\cite{gradescope} for autograding
assignments in the ToC class taught at Northeastern
University, consisting of about 50 students. Before releasing each
assignment, students were introduced to the input format for
specifying their constructions.  We observed that in comparison with
manually graded assignments: (1) autograded assignments had a
significantly higher number of resubmissions, (2) students received
higher grades in the autograded assignments and (3) student feedback
regarding the autograding was overwhelmingly positive due to the
immediate feedback which allowed students to find trivial errors and
helped them better understand the course material.  On an average, more than
95\% of students got full credit on autograded problems, whereas less
than 20\% got full credit on manually graded problems.

\section{Related work}
Automated grading of student submitted automata has been explored in
the past. Alur et. al.~\cite{alur2013automated} focus on techniques
for the partial grading of students' submitted DFA
constructions. D'Antoni et. al.~\cite{dantoni2015how} explore
high-level feedback but it is limited to DFAs. Automata Tutor
v3~\cite{automatatutorv3} is an online platform that automatically
grades and provides feedback on automata, but it is a standalone tool
and cannot be directly integrated within other online platforms such
as Gradescope.

\section{Conclusion and Future work}
We present tools based on the ACL2s theorem prover to check and
provide automatic feedback to students about automata they define. We
believe that a similar approach of theorem prover based feedback
generation can be used for non-programmable assignments in other
subjects like logic and discrete math.

\subsection*{Acknowledgements}
We sincerely thank Mirek Riedewald, the instructor for the ToC course
at Northeastern University in Fall 2019.

\nocite{*}
\bibliographystyle{splncs04}
\bibliography{refs.bib}
\end{document}

